/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SK
 */
@KeywordAnnotation(
        Keyword = "FR6-View Related Findings - Main scenario",
        createNewBrowserInstance = false
)
public class FR6_View_Related_Findings_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_View_Related_Findings_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!View_Related_Findings()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed View Related Findings");
    }

    public boolean View_Related_Findings() {
        //Additional Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.AdditionalInformation())){
            error = "Failed to wait for 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Bowtie_Risk_Assessment_PageObject.AdditionalInformation())){
            error = "Failed to scroll to 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.AdditionalInformation())){
            error = "Failed to click the 'Additional Information' check box.";
            return false;
        }
        narrator.stepPassed("Additional Information check box is displayed");
                    
        //Related Findings panel.
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RelatedFindings_Panel())) {
            error = "Failed to wait for Related Findings panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RelatedFindings_Panel())) {
            error = "Failed to wait for Related Findings panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Related Findings panel is displayed.");
        pause(2000);

        //Available Related Risk Gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RelatedFindings_Openxpath())) {
            error = "Failed to wait for Available Related Findings.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RelatedFindings_Openxpath())) {
            error = "Failed to click Available Related Findings.";
            return false;
        }

        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }  
        
        //Loading data mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.loadingData(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RelatedFindings_OpenedRecordxpath())) {
            error = "Failed to wait for Available Related Risk to open.";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Findings_ProcessFlow())) {
            error = "Fail to wait for Findings process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Findings_ProcessFlow())) {
            error = "Fail to click Findings process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Findings process flow.");
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.findingsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         

        return true;
    }

}
