/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-View Add And Print Bowtie View - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_View_Update_And_Print_Bowtie_View_MainScenario extends BaseClass {

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_View_Update_And_Print_Bowtie_View_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Bowtie()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully viewed and printed bowtie view");
    }

    public boolean View_Bowtie() {
        //View bowtie
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.View_Bowtie())) {
            error = "Failed to wait for 'View bowtie' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.View_Bowtie())) {
            error = "Failed to click on 'View bowtie' button.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'View bowtie' button.");

        String BowtieAssessments = getData("Bowtie Assessments");
        switch (BowtieAssessments) {
            case "Print Bowtie":
                //Print
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Print_Button())) {
                    error = "Failed to wait for 'Print' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Print_Button())) {
                    error = "Failed to click the 'Print' button.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully validated 'Print' button.");

                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Print_Button()))
                {
                    error = "Failed to wait for 'Print' button.";
                    return false;
                }
//                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Print_Button()))
//                {
//
//                    List<By> hello;
//                    hello = SeleniumDriverInstance.shadowLocators_Print_Save_Button();
//                    if (!SeleniumDriverInstance.clickElementByXpathShadow(hello))
//                    {
//                        error = "Failed to click print button";
//                        return false;
//                    }
//
//                    error = "Failed to click the 'Print' button.";
//                    return false;
//                }
                narrator.stepPassedWithScreenShot("Successfully validated 'Print' button.");

                pause(5000);
//                List<By> hello;
//                hello = SeleniumDriverInstance.shadowLocators_Print_Save_Button();
//                if (!SeleniumDriverInstance.clickElementByXpathShadow(hello))
//                {
//                    error = "Failed to click print button";
//                    return false;
//                }

                pause(5000);

                if (!SeleniumDriverInstance.switchToTabOrWindow())
                {
                    error = "Failed to switch to new tab.";
                    return false;
                }
                pause(3000);
                
                if (!SeleniumDriverInstance.clickElementByJavascript("Print"))
                {
                    error = "Failed to click the 'Print' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.pressEnter()) {
                    error = "Failed to press enter";
                    return false;
                }

                if (!SeleniumDriverInstance.switchToTabOrWindow()) {
                    error = "Failed to switch to new tab.";
                    return false;
                }

                if (!SeleniumDriverInstance.printForm()) {
                    error = "Failed to print form";
                    return false;
                }
                if (!SeleniumDriverInstance.switchToTabOrWindow()) {
                    error = "Failed to switch to new tab.";
                    return false;
                }
                SeleniumDriverInstance.pause(5000);
                SeleniumDriverInstance.switchToDefaultContent();
                SeleniumDriverInstance.Driver.close();

                break;

            case "Clear Bowtie":
                //Cause wording
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Cause_Wording())) {
                    error = "Failed to wait for 'Cause' wording";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Cause_Wording())) {
                    error = "Failed to click on 'Cause' wording";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Cause' wording.");

                //Delete button
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Delete_Cause_Button())) {
                    error = "Failed to click on 'Delete' button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Delete' button.");

                SeleniumDriverInstance.switchToTabOrWindow();

                //Confirm dialog
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Yes_Button())) {
                    error = "Failed to wait for Yes Button";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Yes_Button())) {
                    error = "Failed to click on Yes Button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Yes' button.");

                //Switch to frame
                if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to wait for frame ";
                    return false;
                }
                if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to switch to frame ";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully switched to frame");
                SeleniumDriverInstance.switchToTabOrWindow();

                //Ok button
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Ok_Button())) {
                    error = "Failed to wait for OK Button";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Ok_Button())) {
                    error = "Failed to click on OK Button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'OK' button.");

                //Switch to frame
                if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to wait for frame ";
                    return false;
                }
                if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to switch to frame ";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully switched to frame");

                //Cause wording
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Wording())) {
                    error = "Failed to wait for 'Consequence' wording";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Wording())) {
                    error = "Failed to click on 'Consequence' wording";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Consequence' wording.");

                //Delete button
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Delete_Consequence_Button())) {
                    error = "Failed to click on 'Delete' button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Delete' button.");

                SeleniumDriverInstance.switchToTabOrWindow();
                //Confirm dialog
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Yes_Button())) {
                    error = "Failed to wait for Yes Button";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Yes_Button())) {
                    error = "Failed to click on Yes Button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Yes' button.");

                //Switch to frame
                if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to wait for frame ";
                    return false;
                }
                if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to switch to frame ";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully switched to frame");

                SeleniumDriverInstance.switchToTabOrWindow();

                //Ok button
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Ok_Button())) {
                    error = "Failed to wait for OK Button";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Ok_Button())) {
                    error = "Failed to click on OK Button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'OK' button.");

                //Switch to frame
                if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to wait for frame ";
                    return false;
                }
                if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
                    error = "Failed to switch to frame ";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully switched to frame");

                break;

            case "Add Consequences":
                //Consequence block
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Block())) {
                    error = "Failed to wait for 'Consequences' block.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Block())) {
                    error = "Failed to click on 'Consequences' block.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Consequences' block.");

                //Consequence block Plus Button
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Block_Plus_Button())) {
                    error = "Failed to wait for 'Consequences' block Plus Button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Block_Plus_Button())) {
                    error = "Failed to click on 'Consequences' block Plus Button.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Consequences' block Plus Button.");

                //Concequences_Dropdown
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequences_Dropdown())) {
                    error = "Failed to wait for 'Concequences' dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequences_Dropdown())) {
                    error = "Failed to click the 'Concequences' dropdown.";
                    return false;
                }
                
                pause(2000);
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Concequences_Option(getData("Concequences 1")))) {
                    error = "Failed to wait for Concequences Option: " + getData("Concequences 1");
                    return false;
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Concequences_Option(getData("Concequences 1")))) {
                    error = "Failed to double click Concequences Option: " + getData("Concequences 1");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Concequences_Option(getData("Concequences 2")))) {
                    error = "Failed to click Concequences Option: " + getData("Concequences 2");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Control Concequences Option: " + getData("Concequences 1") + " -> " + getData("Concequences 2"));
              break;
              
              case "Add Causes":
                //Cause
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Cause_Wording())) {
                    error = "Failed to wait for 'Cause' wording";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Cause_Wording())) {
                    error = "Failed to click on 'Cause' wording";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Cause' wording.");

                //Plus button
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Add_Cause_Button())) {
                    error = "Failed to click on 'Plus' button";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked 'Plus' wording.");

                //Cause 
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.PleaseSelectControlCategory_Dropdown())) {
                    error = "Failed to wait for Please select dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.PleaseSelectControlCategory_Dropdown())) {
                    error = "Failed to click the Please select dropdown.";
                    return false;
                }
                pause(2000);
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Cause 1")))) {
                    error = "Failed to wait for Cause Option: " + getData("Cause 1");
                    return false;
                }
                if (!SeleniumDriverInstance.doubleClickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Cause 1")))) {
                    error = "Failed to double click Cause Option: " + getData("Cause 1");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Cause 2")))) {
                    error = "Failed to click Cause Option: " + getData("Cause 2");
                    return false;
                }
                 narrator.stepPassedWithScreenShot("Control Cause Option: " + getData("Cause 1") + " -> " + getData("Cause 2"));

                break;

            case "Add Event Consequence":
                //Risk Block
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Block())) {
                    error = "Failed to wait for 'Risk' Block.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Block())) {
                    error = "Failed to click the 'Risk' Block.";
                    return false;
                }

                //Right Consequence Add Button
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RightRisk_AddButton())) {
                    error = "Failed to wait for 'Right Risk' Block.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RightRisk_AddButton())) {
                    error = "Failed to click the 'Right Risk' Block.";
                    return false;
                }

                //Consequence input field
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Input())) {
                    error = "Failed to wait for Consequence input.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Input())) {
                    error = "Failed to click for Consequence input.";
                    return false;
                }
                SeleniumDriverInstance.Driver.findElement(By.xpath(Bowtie_Risk_Assessment_PageObject.Consequence_Input())).clear();
                if (!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.Consequence_Input1(), getData("Consequence description"))) {
                    error = "Failed to click for Consequence input.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Consequence description: " + getData("Consequence description"));
                break;

            case "Add Event Cause":
                 //Risk Block
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Block())) {
                    error = "Failed to wait for 'Risk' Block.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Block())) {
                    error = "Failed to click the 'Risk' Block.";
                    return false;
                }

                //Left Cause Add Button
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.LeftRisk_AddButton())) {
                    error = "Failed to wait for 'Left Risk' Block.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.LeftRisk_AddButton())) {
                    error = "Failed to click the 'Left Risk' Block.";
                    return false;
                }
                
                //Cause input field
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Cause_Input())) {
                    error = "Failed to wait for Cause input.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Cause_Input())) {
                    error = "Failed to click for Cause input.";
                    return false;
                }
                SeleniumDriverInstance.Driver.findElement(By.xpath(Bowtie_Risk_Assessment_PageObject.Cause_Input())).clear();
                if (!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.Cause_Input1(), getData("Cause description"))) {
                    error = "Failed to click for Cause input.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Cause description: " + getData("Cause description"));
                
                break;

            case "Add Preventative Control":

                break;

            case "Add Event":
                break;

            case "Add Cause":
                break;
        }

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.viewBowtie_SaveButton())) {
            error = "Failed to wait for 'Save' Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.viewBowtie_SaveButton())) {
            error = "Failed to click the 'Save' Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Save button.");
        pause(10000);

        //Close the IsoMetrix bowtie risk assessment browser tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Nav_Back())) {
            error = "Failed to wait for 'Back' button";
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Nav_Back())) {
            error = "Failed to click on 'Back' button";
            return false;
        }

        pause(1000);
        //Loading data mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.loadingData(), 40)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //View bowtie
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.View_Bowtie())) {
            error = "Failed to wait for 'View bowtie' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated 'View bowtie' button.");
        narrator.stepPassedWithScreenShot("Successfully Closed the IsoMetrix bowtie risk assessment browser tab.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }

}
