/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Bowtie Risk Assessment - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Bowtie_Risk_Assessment_MainScenario extends BaseClass{
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Bowtie_Risk_Assessment_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!Navigate_TO_Bowtie_Risk_Assessment()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Bowtie_Risk_Assessment()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Communication Groups");
    }

    public boolean Navigate_TO_Bowtie_Risk_Assessment(){
        //Tailings Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.TailingsManagement_Module())){
            error = "Failed to wait for 'Tailings Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.TailingsManagement_Module())){
            error = "Failed to click on 'Tailings Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management' module.");

        //Risk Management module
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskManagement_Module())){
            error = "Failed to wait for 'Risk Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskManagement_Module())){
            error = "Failed to click on 'Risk Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Risk Management' module.");

        //Navigate to Bowtie Risk Assessment
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_Module())){
            error = "Failed to wait for 'Bowtie Risk Assessment' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_Module())){
            error = "Failed to click on 'Bowtie Risk Assessment' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Bowtie Risk Assessment' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Bowtie_Risk_Assessment(){
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BusinessUnit_Dropdown())){
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BusinessUnit_Dropdown())){
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BusinessUnit_Option1(getData("Business Unit 1")))){
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BusinessUnit_Option1(getData("Business Unit 1")))){
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BusinessUnit_Option1(getData("Business Unit 2")))){
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BusinessUnit_Option(getData("Business Unit 3")))){
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_Dropdown())){
            error = "Failed to wait for Impact type dropdown.";
            return false;
        }
        if (getData("Impact type select all").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_Dropdown())){
                error = "Failed to click the Impact type dropdown.";
                return false;
            }
            
            pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_SelectAll())){
                error = "Failed to wait for select all option";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_SelectAll())){
                error = "Failed to click the select all option";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked select all impact type");
        } else {
            String[] Impact_Type = getData("Impact Type").split(",");
            for (int i = 0; i < Impact_Type.length; i++){
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_Dropdown())){
                    error = "Failed to click the Impact type dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_Option(Impact_Type[i]))){
                    error = "Failed to wait for Impact Type option: " + Impact_Type[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_Option(Impact_Type[i]))){
                    error = "Failed to click the Impact Type option: " + Impact_Type[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ImpactType_Dropdown())){
                    error = "Failed to click the Impact type dropdown.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked Impact Type option: " + Impact_Type[i]);
            }
        }

        //Create bowtie assessment from
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieAssessmentFrom_Dropdown())){
            error = "Failed to wait for Create bowtie assessment from dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieAssessmentFrom_Dropdown())){
            error = "Failed to click the Create bowtie assessment from dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieAssessmentFrom_Option(getData("bowtie assessment from")))){
            error = "Failed to wait for Create bowtie assessment from option: " + getData("bowtie assessment from");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieAssessmentFrom_Option(getData("bowtie assessment from")))){
            error = "Failed to click the Create bowtie assessment from option: " + getData("bowtie assessment from");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Create bowtie assessment from option: " + getData("bowtie assessment from"));

        switch (getData("bowtie assessment from")){
            case "New Blank Bowtie Risk Assessment":
                //Risk
                if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Dropdown())){
                    error = "Failed to wait for Risk Dropdown.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Dropdown())){
                    error = "Failed to click Risk Dropdown.";
                    return false;
                }
                if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk")))){
                    error = "Failed to wait fot Risk Option: " + getData("Risk");
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk")))){
                    error = "Failed to click Risk Option: " + getData("Risk");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Risk Option: " + getData("Risk"));
                
                if (getData("Add new").equalsIgnoreCase("Yes")){
                    //Add new
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.AddNew_Button())){
                        error = "Failed to wait for 'Add New' button.";
                        return false;
                    }
                    String addNew = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.AddNew_Button());

                    if (!addNew.equalsIgnoreCase("Add new")){
                        error = "Failed to validate Add New button with " + addNew;
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully validated the Add new button.");

                    parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.AddNew_Button())){
                        error = "Failed to wait for Add new button.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.AddNew_Button())){
                        error = "Failed to click Add new button.";
                        return false;
                    }
                    SeleniumDriverInstance.switchToTabOrWindow();
                    narrator.stepPassedWithScreenShot("Successfully clicked Add new button");

                    //Switch to frame
                    if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeXpath())){
                        error = "Failed to wait for frame ";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())){
                        error = "Failed to switch to frame ";
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully switched to frame");

                    //Process flow
                    pause(8000);
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Library_ProcessFlow())){
                        error = "Failed to wait for 'Process flow' button.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Library_ProcessFlow())){
                        error = "Failed to click on 'Process flow' button.";
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

                    //Risk
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk())){
                        error = "Failed to wait fot Risk field";
                        return false;
                    }
                    if (!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.Risk(), getData("Risk library risk"))){
                        error = "Failed to enter: " + getData("Risk library risk");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully entered: " + getData("Risk library risk"));

                    //Impact type
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_ImpactType_Dropdown())){
                        error = "Failed to click the Impact type dropdown.";
                        return false;
                    }
                    
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_ImpactType_SelectAll())){
                        error = "Failed to wait for select all option";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_ImpactType_SelectAll())){
                        error = "Failed to click the select all option";
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully clicked select all impact type");

                    //Priority unwanted event
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Priority_Unwanted_Event_Dropdown())){
                        error = "Failed to click the Priority unwanted event dropdown.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Priority_Unwanted_Event_Select(getData("Priority unwanted event")))){
                        error = "Failed to wait for option: " + getData("Priority unwanted event");
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Priority_Unwanted_Event_Select(getData("Priority unwanted event")))){
                        error = "Failed to click the option: " + getData("Priority unwanted event");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully clicked option: " + getData("Priority unwanted event"));

                    //Related critical conrol inspection
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Related_Critical_Control_Inspetion_Dropdown())){
                        error = "Failed to click the Related critical conrol inspection dropdown.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Related_Critical_Control_Inspetion_Select(getData("Related critical control inspection")))){
                        error = "Failed to wait for option: " + getData("Related critical control inspection");
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Related_Critical_Control_Inspetion_Select(getData("Related critical control inspection")))){
                        error = "Failed to click the option: " + getData("Related critical control inspection");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully clicked option: " + getData("Related critical control inspection"));

                    //Save Button
                    if (!getData("Save To Continue").equalsIgnoreCase("Yes")){
                        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_SaveButton())){
                            error = "Failed to wait for Save Button";
                            return false;
                        }
                        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_SaveButton())){
                            error = "Failed to click on Save Button";
                            return false;
                        }
                    } else {
                        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_SaveToContinue_Button())){
                            error = "Failed to wait for Save to continue Button";
                            return false;
                        }
                        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskLibrary_SaveToContinue_Button())){
                            error = "Failed to click on Save to continue Button";
                            return false;
                        }
                    }

                    pause(3000);

                    //Validate if the record has been saved or not.
                    if (!SeleniumDriverInstance.waitForElementsByXpath(Bowtie_Risk_Assessment_PageObject.validateSave())){
                        error = "Failed to wait for Save validation.";
                        return false;
                    }

                    String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.validateSave());

                    if (!SaveFloat.equals("Record saved")){
                        narrator.stepPassedWithScreenShot("Failed to save record.");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

                    SeleniumDriverInstance.switchToDefaultContent();
                    SeleniumDriverInstance.Driver.close();
                    SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

                    //Switch to frame
                    if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeXpath())){
                        error = "Failed to wait for frame ";
                        return false;
                    }
                    if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())){
                        error = "Failed to switch to frame ";
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully switched to frame");
                    
                    //Risk
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Dropdown())){
                        error = "Failed to wait for Risk Dropdown.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Dropdown())){
                        error = "Failed to click Risk Dropdown.";
                        return false;
                    }
                    
                    pause(2000);
                    if (!SeleniumDriverInstance.scrollToElement(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk library risk")))){
                        error = "Failed to scroll to Risk Option: " + getData("Risk library risk");
                        return false;
                    }
                    
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk library risk")))){
                        error = "Failed to wait for Risk Option: " + getData("Risk library risk");
                        return false;
                    }
                    
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk library risk")))){
                        error = "Failed to click Risk Option: " + getData("Risk library risk");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Risk Option: " + getData("Risk library risk"));

                } else {
                    //Risk
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Dropdown())){
                        error = "Failed to wait for Risk Dropdown.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Dropdown())){
                        error = "Failed to click Risk Dropdown.";
                        return false;
                    }
                    
                    pause(2000);
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk")))){
                        error = "Failed to wait fot Risk Option: " + getData("Risk");
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Risk_Option(getData("Risk")))){
                        error = "Failed to click Risk Option: " + getData("Risk");
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Risk Option: " + getData("Risk"));

                    //Add new
                    if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.AddNew_Button())){
                        error = "Failed to wait for 'Add New' button.";
                        return false;
                    }
                    String addNew = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.AddNew_Button());

                    if(!addNew.equalsIgnoreCase("Add new")){
                        error = "Failed to validate Add New button with " + addNew;
                        return false;
                    }
                    narrator.stepPassedWithScreenShot("Successfully validated the Add new button.");
                }
                break;

            case "Create From Previous Bowtie Risk Assessment":
                //Previous bowtie risk reference
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.PreviousBowtieRisk_Dropdown())){
                    error = "Failed to wait for Previous bowtie risk reference Dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.PreviousBowtieRisk_Dropdown())){
                    error = "Failed to click Previous bowtie risk reference Dropdown.";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.PreviousBowtieRisk_Option(getData("Previous bowtie risk")))){
                    error = "Failed to wait fot Previous bowtie risk reference Option: " + getData("Previous bowtie risk");
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.PreviousBowtieRisk_Option(getData("Previous bowtie risk")))){
                    error = "Failed to click Previous bowtie risk reference Option: " + getData("Previous bowtie risk");
                    return false;
                }
                narrator.stepPassedWithScreenShot("Previous bowtie risk reference Option: " + getData("Previous bowtie risk"));

                break;

            case "Create From Previous Integrated Risk Assessment":
                narrator.stepPassedWithScreenShot("Create From Previous Integrated Risk Assessment successfully displayed");
                break;

            default:
                System.out.println(" Sorry we don't have any other options");

        }

        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.SaveButton())){
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.SaveButton())){
                error = "Failed to click on Save Button";
                return false;
            }
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.SaveToContinue_Button())){
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.SaveToContinue_Button())){
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Bowtie_Risk_Assessment_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
       
        if(getData("bowtie assessment from").equalsIgnoreCase("Create From Previous Bowtie Risk Assessment")||getData("bowtie assessment from").equalsIgnoreCase("Create From Previous Integrated Risk Assessment")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskDescription())){
                error = "Failed to wait for 'Bowtie risk Description'.";
                return false;
            }
            if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskDescription(), getData("Bowtie Risk Description"))){
                error = "Failed to click on 'Bowtie risk Description': " + getData("Bowtie Risk Description");
                return false;
            }
            pause(1000);
            narrator.stepPassedWithScreenShot("Successfully entered to 'Bowtie risk Description': " + getData("Bowtie Risk Description"));
            
        }
        
        //Risk source
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskSource_Dropwown())){
            error = "Failed to wait for 'Risk source' dropwown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskSource_Dropwown())){
            error = "Failed to click on 'Risk source' dropwown.";
            return false;
        }
        
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskSource_SelectAll())){
            error = "Failed to click on 'Risk source' Select All.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully Selected All Risk Source.");
        
        //Bowtie risk reference
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskReference())){
            error = "Failed to wait for 'Bowtie risk reference'.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskReference(), getData("Bowtie Risk Reference"))){
            error = "Failed to click on 'Bowtie risk reference'.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully entered to 'Bowtie risk reference': " + getData("Bowtie Risk Reference"));
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.SaveButton())) {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.SaveButton())) {
            error = "Failed to click on Save Button";
            return false;
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 400)){
            error = "Website too long to load wait reached the time out";
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.bowtieRiskAssessRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         

        return true;
    }

}
