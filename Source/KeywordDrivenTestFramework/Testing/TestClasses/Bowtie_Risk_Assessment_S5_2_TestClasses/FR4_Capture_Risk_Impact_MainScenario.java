/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR4-Capture Risk Impact - Main scenario",
        createNewBrowserInstance = false
)

public class FR4_Capture_Risk_Impact_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR4_Capture_Risk_Impact_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!Navigate_To_Risk_Impact()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Risk_Impact()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Risk Impact");
    }
    
    public boolean Navigate_To_Risk_Impact(){
//        //Bowtie Risk Assessment - Bowtie Consequences
//        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_CloseButton())){
//            error = "Failed to wait for Bowtie Risk Assessment - Bowtie Consequences Close Button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_CloseButton())){
//            error = "Failed to click the Bowtie Risk Assessment - Bowtie Consequences Close Button.";
//            return false;
//        }
        pause(4000);
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        //Risk Assessment Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskAssessment_Panel())){
            error = "Failed to wait for 'Risk Assessment' Panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskAssessment_Panel())){
            error = "Failed to click on 'Risk Assessment' Panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Risk Assessment' Panel.");

        //Risk Impact Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Panel())){
            error = "Failed to wait for 'Risk Impact' Panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Panel())){
            error = "Failed to click on 'Risk Impact' Panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Risk Impact' Panel.");
        
        //Risk Impact Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_AddButton())){
            error = "Failed to wait for Risk Impact Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_AddButton())){
            error = "Failed to click the Risk Impact Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Risk Impact Add Button.");
                
        return true;
    }

    public boolean Capture_Risk_Impact(){
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(3000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
       //Impact type
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Dropdown())){
           error = "Failed to wait for Impact type dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Dropdown())){
           error = "Failed to click for the Impact type dropdown.";
           return false;
       }
       pause(1000);
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Option(getData("Impact Type")))){
           error = "Failed to wait for Impact type option: " + getData("Impact Type");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Option(getData("Impact Type")))){
           error = "Failed to click the Impact type option: " + getData("Impact Type");
           return false;
       }
       narrator.stepPassedWithScreenShot("Impact type option: " + getData("Impact Type"));
       
       //Impact
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Impact_Dropdown())){
           error = "Failed to wait for Impact dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Impact_Dropdown())){
           error = "Failed to click the Impact dropdown.";
           return false;
       }
       pause(1000);
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Impact_Option(getData("Impact 1")))){
           error = "Failed to wait for Impact option: " + getData("Impact 1");
           return false;
       }
       if(!SeleniumDriverInstance.doubleClickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Impact_Option(getData("Impact 1")))){
           error = "Failed to wait for Impact option: " + getData("Impact 1");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Impact_Option(getData("Impact 2")))){
           error = "Failed to wait for Impact option: " + getData("Impact 2");
           return false;
       }
       narrator.stepPassedWithScreenShot("Impact option: " + getData("Impact 1") + " -> " + getData("Impact 2"));
       
       //Likelihood rating
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.LikelihoodRating_Dropdown())){
           error = "Failed to wait for Likelihood rating dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.LikelihoodRating_Dropdown())){
           error = "Failed to click the Likelihood rating dropdown.";
           return false;
       }
       pause(1000);
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Impact_Option(getData("Likelihood rating")))){
           error = "Failed to wait for Likelihood rating option: " + getData("Likelihood rating");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Impact_Option(getData("Likelihood rating")))){
           error = "Failed to wait for Likelihood rating option: " + getData("Likelihood rating");
           return false;
       }
       narrator.stepPassedWithScreenShot("Likelihood rating: " + getData("Likelihood rating"));
       
       //Impact description
       if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.ImpactDescription(), getData("Impact description"))){
           error = "Failed to wait for Impact description input.";
           return false;
       }
       narrator.stepPassedWithScreenShot("Successfully entered Impact description: " + getData("Impact description"));
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_SaveButton())) {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_SaveButton())) {
            error = "Failed to click on Save Button";
            return false;
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Bowtie_Risk_Assessment_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.riskImpactRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
        
        //Risk Impact - Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_CloseButton())){
            error = "Failed to wait for Bowtie Risk Assessment - Risk Impact Close Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_CloseButton())){
            error = "Failed to click the Bowtie Risk Assessment - Risk Impact Close Button.";
            return false;
        }
        pause(4000);
        
        //Risk Impact
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RiskImpact_Record(getData("Impact Type")))){
            error = "Failed to wait for Risk Impact - Impact Type Record: " + getData("Impact Type");
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Bowtie_Risk_Assessment_PageObject.RiskAssessment_Panel())){
            error = "Failed to click on 'Risk Assessment' Panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot(getData("Impact Type") + " - Record Successfully created.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        return true;
    }

}
