/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-View Update and Print Bowtie View - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR5_View_Update_And_Print_Bowtie_View_AlternateScenario1 extends BaseClass {

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_View_Update_And_Print_Bowtie_View_AlternateScenario1() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Bowtie()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully viewed and fliped bowtie view");
    }

    public boolean View_Bowtie() {
        //View bowtie
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.View_Bowtie())) {
            error = "Failed to wait for 'View bowtie' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.View_Bowtie())) {
            error = "Failed to click on 'View bowtie' button.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'View bowtie' button.");

        //Flip
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.flipBowtie())) {
            error = "Failed to wait for Flip button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.flipBowtie())) {
            error = "Failed to click on Flipbutton.";
            return false;
        }
        
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click Flip button.");

        return true;
    }

}
