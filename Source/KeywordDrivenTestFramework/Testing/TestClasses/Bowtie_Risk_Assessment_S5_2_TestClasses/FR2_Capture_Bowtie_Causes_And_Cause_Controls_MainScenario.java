/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR2-Capture Bowtie Causes and Cause Controls - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Bowtie_Causes_And_Cause_Controls_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Bowtie_Causes_And_Cause_Controls_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!Navigate_TO_Causes_Panel()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Bowtie_Causes()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Bowtie Causes");
    }
    
    public boolean Navigate_TO_Causes_Panel(){
        //Causes Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Causes_Panel())){
            error = "Failed to wait for 'Causes' Panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Causes_Panel())){
            error = "Failed to click on 'Causes' Panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Causes' Panel.");

               
        //Bowtie Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_AddButton())){
            error = "Failed to wait for Bowtie Cause Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_AddButton())){
            error = "Failed to click the Bowtie Cause Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Bowtie Causes Add Button.");
        pause(8000);        
        return true;
    }

    public boolean Capture_Bowtie_Causes(){
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_BowtieCauses_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_BowtieCauses_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
       //Cause
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Cause())){
           error = "Failed to wait for Cause input.";
           return false;
       }
       if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.Cause(), getData("Cause"))){
           error = "Failed to wait for Cause input.";
           return false;
       }
       narrator.stepPassedWithScreenShot("Successfully entered Cause: " + getData("Cause"));
        
        //Save Button
        if(!getData("Save To Continue").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCause_SaveButton())) {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCause_SaveButton())) {
                error = "Failed to click on Save Button";
                return false;
            }
        } else{
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCause_SaveToContinue_Button())) {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCause_SaveToContinue_Button())) {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Bowtie_Risk_Assessment_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.bowtieCausesRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
       
        //Bowtie Preventative Controls
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_Panel())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_Panel())){
                error = "Failed to wait for Bowtie Preventative Controls Panel.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully displayed Bowtie Preventative Controls Panel");
        
        //Bowtie cause
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_CloseButton())){
            error = "Failed to wait for Bowtie Causes Close Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_CloseButton())){
            error = "Failed to click the Bowtie Causes Close Button.";
            return false;
        }
        pause(4000);
        
        //Bowtie Causes
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_Record(getData("Cause")))){
            error = "Failed to wait for Bowtie Causes Record.";
            return false;
        }
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        return true;
    }

}
