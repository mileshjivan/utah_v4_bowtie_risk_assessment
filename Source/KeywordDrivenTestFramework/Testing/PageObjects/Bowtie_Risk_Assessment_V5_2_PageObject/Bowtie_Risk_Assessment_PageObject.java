/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;

/**
 *
 * @author skhumalo
 */
public class Bowtie_Risk_Assessment_PageObject
{

    public static String Concequences_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Consequence_Block_Plus_Button()
    {
        return "//div[@title='Consequence']/../..//a[@class='item add bRight']";
    }

    public static String Consequence_Input()
    {
        return "(//div[@title='Consequence']/..//..//div[text()='...'])[1]";
    }

    public static String Consequence_Input1()
    {
        return "(//div[@title='Consequence']/..//..//div[text()=' '])[1]";
    }
    
    public static String Cause_Input()
    {
        return "(//div[@title='Cause']/..//..//div[text()='...'])[1]";
    }
    
    public static String Cause_Input1()
    {
        return "(//div[@title='Cause']/..//..//div[text()=' '])[1]";
    }
    
    

    public static String Consequences_Dropdown()
    {
        return "//div[@class='block cControl jtk-endpoint-anchor jtk-connected']//div[text()='Please select']";
    }

    public static String IncidentManagement_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String LeftRisk_AddButton()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bLeft']";
    }

    public static String RightRisk_AddButton()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bRight']";
    }

    public static String Risk_Block()
    {
        return "//div[@title='Risk']";
    }

    public static String loadingData()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";
    }

    public static String AddNew_Button()
    {
        return "//div[@id='control_8CB8326E-D502-43AE-97BC-424D6D869C5C']//div//div";
    }

    public static String AdditionalInformation()
    {
        return "//div[@id='control_BC6C5FE8-5348-4CC0-942E-85FA43E70F99']//div[@class='c-chk']";
    }

    public static String BowtieAssessmentFrom_Dropdown()
    {
        return "//div[@id='control_4835153C-D8B0-4056-BA6E-146F58B04AA6']";
    }

    public static String BowtieAssessmentFrom_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String BowtieCauses_AddButton()
    {
        return "//div[@id='control_A2CFC545-8B8A-4063-A5F7-21A7B295D1C9']//div[text()='Add']";
    }

    public static String BowtieCauses_BowtiePreventativeControls_AddButton()
    {
        return "//div[@id='control_8AB00884-981A-40FB-A6C4-A4F2D21DF6F8']//div[text()='Add']";
    }

    public static String BowtieCauses_BowtieCorrectiveControls_AddButton()
    {
        return "//div[@id='control_D4D30DB4-E22E-43AE-8955-88C43147D350']//div[text()='Add']";
    }

    public static String BowtieCauses_BowtiePreventativeControls_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']";
    }

    public static String BowtieCauses_CloseButton()
    {
        return "(//div[@id='form_94AE510C-C2B9-4C21-AE06-AD657949590F']//i[@class='close icon cross'])[1]";
    }

    public static String BowtieCauses_Record(String data)
    {
        return "(//div[@id='control_A2CFC545-8B8A-4063-A5F7-21A7B295D1C9']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtieConsequences_AddButton()
    {
        return "//div[@id='control_B37149EB-3023-4C48-89E7-3ADF2ABB5382']//div[text()='Add']";
    }

    public static String BowtieConsequences_CloseButton()
    {
        return "(//div[@id='form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']//i[@class='close icon cross'])[1]";
    }

    public static String BowtieConsequences_Panel()
    {
        return "//div[text()='Bowtie Consequences']";
    }

    public static String BowtieConsequences_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']";
    }

    public static String BowtieConsequences_Record(String data)
    {
        return "(//div[@id='control_B37149EB-3023-4C48-89E7-3ADF2ABB5382']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtieCorrectiveControls_CloseButton()
    {
        return "(//div[@id='form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']//i[@class='close icon cross'])[1]";
    }

    public static String BowtieCorrectiveControls_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']";
    }

    public static String BowtieCorrectiveControls_Record(String data)
    {
        return "(//div[@id='control_D4D30DB4-E22E-43AE-8955-88C43147D350']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtieCorrectiveControls_SaveButton()
    {
        return "//div[@id='btnSave_form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']";
    }

    public static String BowtieCorrectiveControls_SaveToContinue_Button()
    {
        return "//div[@id='control_05221A5C-0B3F-4FE2-BD8B-90D5FCF00322']";
    }

    public static String BowtiePreventativeControls_CloseButton()
    {
        return "(//div[@id='form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']//i[@class='close icon cross'])[1]";
    }

    public static String BowtiePreventativeControls_Panel()
    {
        return "//div[text()='Bowtie Preventative Controls']";
    }

    public static String BowtieCorrectiveControls_Panel()
    {
        return "//div[text()='Bowtie Corrective Controls']";
    }

    public static String BowtiePreventativeControls_Record(String data)
    {
        return "(//div[@id='control_8AB00884-981A-40FB-A6C4-A4F2D21DF6F8']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtiePreventativeControls_SaveButton()
    {
        return "//div[@id='btnSave_form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']";
    }

    public static String BowtiePreventativeControls_SaveToContinue_Button()
    {
        return "//div[@id='control_05221A5C-0B3F-4FE2-BD8B-90D5FCF00322']";
    }

    public static String BowtieRiskAssessment_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String BowtieRiskAssessment_BowtieCauses_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_94AE510C-C2B9-4C21-AE06-AD657949590F']";
    }

    public static String BowtieRiskAssessment_Module()
    {
        return "//label[text()='Bowtie Risk Assessment']";
    }

    public static String BowtieRiskAssessment_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_E5D05DD0-7E8F-4A41-9077-0A1F8DB472E7']";
    }

    public static String BowtieRiskDescription()
    {
        return "//div[@id='control_A6CF6BF0-6F8A-4417-9C05-61B1A2795897']//textarea";
    }

    public static String BowtieRiskReference()
    {
        return "(//div[@id='control_DF2197C5-845E-4592-91EC-5212967FDDD4']//input)[1]";
    }

    public static String BusinessUnit_Dropdown()
    {
        return "//div[@id='control_555ECA88-A71E-4EFF-8FCC-876AA6EA64A4']";
    }

    public static String BusinessUnit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String BusinessUnit_Option1(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String Cause()
    {
        return "(//div[@id='control_16ADAF90-6D9A-403B-BA0C-F8D41353A9E2']//input)[1]";
    }

    public static String Causes_Panel()
    {
        return "//div[@id='control_271E1CF4-0933-4031-B490-D2E20691348B']";
    }

    public static String Consequences()
    {
        return "(//div[@id='control_5858AB70-A475-47D2-A794-155CFC765C37']//input)[1]";
    }

    public static String Consequences_Panel()
    {
        return "//span[text()='Consequences']";
    }

    public static String Consequences_SaveButton()
    {
        return "//div[@id='btnSave_form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']";
    }

    public static String Consequences_SaveToContinue_Button()
    {
        return "//div[@id='control_F4649EEC-490F-4C21-B41D-75E275CB2D16']";
    }

    public static String ControlCategory_Dropdown()
    {
        return "//div[@id='control_B5DF4C13-9442-46D9-8FF0-C7E70CF2E7DC']";
    }

    public static String ControlCategory_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String ControlDescription()
    {
        return "(//div[@id='control_6754FD09-D78D-4DE7-A588-FDDFDCCC78F5']//input)[1]";
    }

    public static String ControlImplementation_Dropdown()
    {
        return "//div[@id='control_6A6FD9CA-8F86-4108-B9FF-E64096C8C47C']";
    }

    public static String ControlMatrix_Panel()
    {
        return "//span[text()='Control Matrix']";
    }

    public static String ControlOwner_Dropdown()
    {
        return "//div[@id='control_7D9D2EE5-75F6-4F44-8896-C03AFF83F7F7']";
    }

    public static String ControlQuality_Dropdown()
    {
        return "//div[@id='control_EE4E7BE4-1899-40AD-B337-94621B1ABEDF']";
    }

    public static String ControlReference()
    {
        return "//div[@id='control_8432E1DB-25DB-4BC8-812B-8547EFD7B6B7']//input";
    }

    public static String ControlsChecklist_Panel()
    {
        return "//div[text()='Control Checklist']";
    }

    public static String Findings_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_D25EA3B6-2F41-4F02-8356-D079FB56E1D2']";
    }

    public static String ImpactDescription()
    {
        return "//div[@id='control_3D3E68DC-D7A2-429A-8F4C-9E0F537D5768']//textarea";
    }

    public static String ImpactType_Dropdown()
    {
        return "//div[@id='control_F065857D-D6E8-46E9-8C2C-63A9D585AC90']";
    }

    public static String ImpactType_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String ImpactType_SelectAll()
    {
        return "//div[@id='control_F065857D-D6E8-46E9-8C2C-63A9D585AC90']//b[@class='select3-all']";
    }

    public static String Impact_Dropdown()
    {
        return "(//div[@id='control_002897B8-C514-43A0-8A4D-AB34EBE5AC2F']//div)[1]";
    }

    public static String Impact_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String LikelihoodRating_Dropdown()
    {
        return "(//div[@id='control_2EBB8D75-E591-409C-B0E0-719BC842BA8E']//div)[1]";
    }

    public static String PreviousBowtieRisk_Dropdown()
    {
        return "//div[@id='control_682882C1-711D-4069-B623-41C360C3D106']";
    }

    public static String PreviousBowtieRisk_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String RelatedFindings_OpenedRecordxpath()
    {
        return "(//div[@id='form_D25EA3B6-2F41-4F02-8356-D079FB56E1D2']//div[@class='record'])[1]";
    }

    public static String RelatedIncidents_OpenedRecordxpath()
    {
        return "(//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='record'])[1]";
    }

    public static String RelatedFindings_Openxpath()
    {
        return "((//div[@id='control_594E000D-4925-4518-928E-0FB163B28F3B']//div//table)[3]//tr)[1]";
    }

    public static String RelatedIncidents_Openxpath()
    {
        return "((//div[@id='control_2C80A486-F170-4B3F-8C6E-A61FCAC6DA5D']//div//table)[3]//tr)[1]";
    }

    public static String RelatedFindings_Panel()
    {
        return "//span[text()='Related Findings']";
    }

    public static String RelatedIncidents_Panel()
    {
        return "//span[text()='Related Incidents']";
    }

    public static String RiskAssessment_Panel()
    {
        return "//span[text()='Risk Assessment']";
    }

    public static String RiskImpact_AddButton()
    {
        return "//div[text()='Risk Impact']//..//div[text()='Add']";
    }

    public static String RiskImpact_CloseButton()
    {
        return "(//div[@id='form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']//i[@class='close icon cross'])[1]";
    }

    public static String RiskImpact_Dropdown()
    {
        return "//div[@id='control_ECDA14DC-13FD-4511-B373-23EEB01C7F01']";
    }

    public static String RiskImpact_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String RiskImpact_Panel()
    {
        return "//div[text()='Risk Impact']";
    }

    public static String RiskImpact_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']";
    }

    public static String RiskImpact_Record(String data)
    {
        return "(//div[@id='control_9C30C831-F4CA-4423-B4A3-5A8A69011966']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String RiskImpact_SaveButton()
    {
        return "//div[@id='btnSave_form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']";
    }

    public static String RiskManagement_Module()
    {
        return "//label[text()='Risk Management']";
    }

    public static String RiskSource_Dropwown()
    {
        return "//div[@id='control_93098483-FC5B-46E1-91DF-CAFA0061578D']";
    }

    public static String RiskSource_SelectAll()
    {
        return "//div[@id='control_93098483-FC5B-46E1-91DF-CAFA0061578D']//b[@class='select3-all']";
    }

    public static String Risk_Dropdown()
    {
        return "//div[@id='control_B77E0F46-408A-46DA-8C60-F963B8AA9E7C']";
    }

    public static String Risk_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String SaveButton()
    {
        return "//div[@id='btnSave_form_E5D05DD0-7E8F-4A41-9077-0A1F8DB472E7']";
    }

    public static String bowtieRiskAssessRecordNumber_xpath() {
        return "(//div[@id='form_E5D05DD0-7E8F-4A41-9077-0A1F8DB472E7']//div[contains(text(),'- Record #')])[1]";
    }

    public static String bowtieCausesRecordNumber_xpath() {
        return "(//div[@id='form_94AE510C-C2B9-4C21-AE06-AD657949590F']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String bowtiePreventiveCntlRecordNumber_xpath() {
        return "(//div[@id='form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']//div[contains(text(),'- Record #')])[1]";
    } 
    
    public static String bowtieConsequencesRecordNumber_xpath() {
        return "(//div[@id='form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']//div[contains(text(),'- Record #')])[1]";
    } 
    
    public static String bowtieCorrectiveCntlNumber_xpath() {
        return "(//div[@id='form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']//div[contains(text(),'- Record #')])[1]";
    } 
    
    public static String riskImpactRecordNumber_xpath() {
        return "(//div[@id='form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']//div[contains(text(),'- Record #')])[1]";
    } 
    
    public static String findingsRecordNumber_xpath() {
        return "(//div[@id='form_D25EA3B6-2F41-4F02-8356-D079FB56E1D2']//div[contains(text(),'- Record #')])[1]";
    } 
    
    public static String relatedIncidentsRecordNumber_xpath() {
        return "(//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[contains(text(),'- Record #')])[1]";
    } 
    
            
    public static String BowtieCause_SaveButton()
    {
        return "//div[@id='btnSave_form_94AE510C-C2B9-4C21-AE06-AD657949590F']";
    }

    public static String BowtieCause_SaveToContinue_Button()
    {
        return "//div[@id='control_F1E2583E-95B3-42DA-A6F1-1C1B60C981DE']";
    }

    public static String SaveToContinue_Button()
    {
        return "//div[@id='control_84DBED5A-F603-418D-BED1-4DF4B3562F5B']";
    }

    public static String TailingsManagement_Module()
    {
        return "//label[text()='Tailings Management']";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String Risk_Description()
    {
        return "//div[@id='control_A6CF6BF0-6F8A-4417-9C05-61B1A2795897']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Previous_Bowtie_Risk_Reference_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[1]";
    }

    public static String Risk_Resource()
    {
        return "//div[@id='control_93098483-FC5B-46E1-91DF-CAFA0061578D']//b[@class='select3-all']";
    }

    public static String Bowtie_Risk_Reference()
    {
        return "//div[@id='control_DF2197C5-845E-4592-91EC-5212967FDDD4']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Risk_Library_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_75490176-392D-464D-812B-E3F9E9AC942D']";
    }

    public static String Risk()
    {
        return "//div[@id='control_38779557-7763-43A8-A0EF-A5BC98AAF5E7']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String RiskLibrary_ImpactType_Dropdown()
    {
        return "//div[@id='control_3B7B1EE5-F549-4C34-853A-C8218D87BC29']";
    }

    public static String RiskLibrary_ImpactType_SelectAll()
    {
        return "//div[@id='control_3B7B1EE5-F549-4C34-853A-C8218D87BC29']//b[@class='select3-all']";
    }

    public static String Priority_Unwanted_Event_Dropdown()
    {
        return "//div[@id='control_D33817E8-42C9-4AE4-B2D1-00ADD1A8FBE4']";
    }

    public static String Priority_Unwanted_Event_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String Related_Critical_Control_Inspetion_Dropdown()
    {
        return "//div[@id='control_48E40813-477B-4D8F-968E-0012A53596BC']";
    }

    public static String Related_Critical_Control_Inspetion_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String RiskLibrary_SaveButton()
    {
        return "//div[@id='btnSave_form_75490176-392D-464D-812B-E3F9E9AC942D']";
    }

    public static String RiskLibrary_SaveToContinue_Button()
    {
        return "//div[@id='control_394A6057-B468-40D6-8EEB-858789770EA1']";
    }

    public static String View_Bowtie()
    {
        return "(//div[@id='control_A11FB77F-6183-4BEE-A3AF-9C1E946082FF']//div)[1]";
    }

    public static String flipBowtie()
    {
        return "//div[@id='btnFlip']";
    }

    
    public static String Print()
    {
        return "//div[@title='Print']";
    }

    public static String Print_Button()
    {
        return "//div[@id='btnPrint']";
    }

    public static String Nav_Back()
    {
        return "//div[@id='bowtieNav']//i[@class='back icon arrow-left']";
    }

    public static String Add_Cause_Button()
    {
        return "//div[@title='Cause']/../..//a[@class='item add bLeft']";
    }

    public static String Cause_Wording()
    {
        return "//div[@title='Cause']";
    }

    public static String PleaseSelectControlCategory_Dropdown()
    {
        return "//div[@class='block pControl jtk-endpoint-anchor jtk-connected']//div[@title='Please select']";
    }

    public static String Consequence_Block()
    {
        return "//div[@title='Consequence']";
    }

    public static String viewBowtie_SaveButton()
    {
        return "//div[@id='act_bowtie_right']//div[text()='Save']";
    }

    public static String Control_Description()
    {
        return "//div[text()='...']";
    }

    public static String Save_Button()
    {
        return "//div[@id='btnSave']";
    }

    public static String Control__Description()
    {
        return "//div[@title='Please select']/../..//div[@class='content']";
    }

    public static String Delete_Cause_Button()
    {
        return "//div[@title='Cause']/../..//a[@class='item delete']";
    }

    public static String Yes_Button()
    {
        return "//div[@class='confirm-popup popup']//div[text()='Yes']";
    }

    public static String Ok_Button()
    {
        return "//div[text()='Record has successfully been deleted']/..//div[@id='btnHideAlert']";
    }

    public static String Consequence_Wording()
    {
        return "//div[@title='Consequence']";
    }

    public static String Delete_Consequence_Button()
    {
        return "//div[@title='Consequence']/../..//a[@class='item delete']";
    }

    public static String Event_Left_Plus_Button()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bLeft']";
    }

    public static String Cause_Description()
    {
        return "(//div[@title='Cause']/../..//div[@class='content'])[2]";
    }

    public static String Event_Block()
    {
        return "//div[@title='Risk']";
    }

    public static String Cause__Description()
    {
        return "//div[@title='Please select']/../..//div[@class='content']";
    }

    public static String Flip_Button()
    {
        return "//div[@id='btnFlip']";
    }

    public static String Event_Right_Plus_Button()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bRight']";
    }

    public static String Consequence_Description()
    {
        return "(//div[@title='Consequence']/../..//div[@class='content'])[2]";
    }

    public static String Print_Frame()
    {
        return "//html[@class='']//print-preview-app";
    }

    public static String si_location()
    {
        return "Print.PNG";
    }

    public static String printPreview()
    {
        return "//print-preview-app";
    }

    public static List<By> shadowLocators_Print_Div()
    {
        List<By> locators = new ArrayList<>();
        locators.add(By.tagName("print-preview-app"));
        locators.add(By.cssSelector("print-preview-preview-area"));
        locators.add(By.cssSelector("iframe"));
        return locators;
    }

    public static List<By> shadowLocators_SaveAsPDF()
    {
        List<By> locators = new ArrayList<>();
        locators.add(By.tagName("print-preview-app"));
        locators.add(By.cssSelector("print-preview-destination-settings"));
        locators.add(By.cssSelector("print-preview-destination-dialog"));
        locators.add(By.id("printList"));
        locators.add(By.cssSelector("print-preview-destination-list-item"));
        return locators;
    }

    public static List<By> shadowLocators_ChangeButton()
    {
        List<By> locators = new ArrayList<>();
        locators.add(By.tagName("print-preview-app"));
        locators.add(By.cssSelector("print-preview-destination-settings"));
//        locators.add(By.tagName("print-preview-settings-section"));
        locators.add(By.cssSelector("paper-button"));
        return locators;
    }

    public static List<By> shadowLocators_Print_Save_Button()
    {
        List<By> locators = new ArrayList<>();
        locators.add(By.tagName("print-preview-app"));
        locators.add(By.cssSelector("print-preview-header"));
        locators.add(By.cssSelector("paper-button"));
        return locators;
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String Permit_To_Work_Module()
    {
        return "//label[text()='Permit to Work']";
    }

}
