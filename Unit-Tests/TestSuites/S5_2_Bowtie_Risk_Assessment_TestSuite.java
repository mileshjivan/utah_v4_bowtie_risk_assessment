/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SKhumalo
 */
public class S5_2_Bowtie_Risk_Assessment_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_Bowtie_Risk_Assessment_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;

        //*******************************************
    }

    //S5_2_Communication_Manager_QA01S5_2
    @Test
    public void S5_2_Capture_Bowtie_Risk_Assessment_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Bowtie_Risk_Assessment_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR1-Capture Bowtie Risk Assessment - Main Scenario
    @Test
    public void FR1_Capture_Bowtie_Risk_Assessment_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR1-Capture Bowtie Risk Assessment - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Bowtie_Risk_Assessment_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR1-Capture Bowtie Risk Assessment - Alternate scenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Bowtie_Risk_Assessment_Alternate_Scenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR1-Capture Bowtie Risk Assessment - Alternate scenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Bowtie_Risk_Assessment_Alternate_Scenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR1-Capture Bowtie Risk Assessment - Alternate scenario3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2-Capture Bowtie Causes and Cause Controls - Main Scenario
    @Test
    public void FR2_Capture_Bowtie_Causes_And_Cause_Controls_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR2-Capture Bowtie Causes and Cause Controls - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //UC BRM 02-02-Capture Preventative Controls - Main scenario
    @Test
    public void UC_BRM02_02_Capture_Preventative_Controls_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\UC BRM 02-02-Capture Preventative Controls - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3-Capture Bowtie Consequences and Corrective Controls - Main Scenario
    @Test
    public void FR3_Capture_Bowtie_Consequences_and_Corrective_Controls_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR3-Capture Bowtie Consequences and Corrective Controls - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //UC BRM 03-02: Capture Corrective Controls - Main scenario
    @Test
    public void UC_BRM03_03_Capture_Corrective_Controls_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\UC BRM 03-03-Capture Corrective Controls - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR4-Capture Risk Impact - Main scenario
    @Test
    public void FR4_Capture_Risk_Impact_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR4-Capture Risk Impact - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR5-View Update And Print Bowtie View - Main Scenario
    @Test
    public void FR5_View_Update_And_Print_Bowtie_View_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR5-View Update and Print Bowtie View - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR5_View_Update_And_Print_Bowtie_View_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR5-View Update and Print Bowtie View - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR5_View_Update_And_Print_Bowtie_View_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR5-View Update and Print Bowtie View - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR5_View_Update_And_Print_Bowtie_View_AlternateScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR5-View Update and Print Bowtie View - Alternate Scenario 3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    @Test
    public void FR5_View_Update_And_Print_Bowtie_View_AlternateScenario4() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR5-View Update and Print Bowtie View - Alternate Scenario 4.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR5_View_Update_And_Print_Bowtie_View_AlternateScenario5() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR5-View Update and Print Bowtie View - Alternate Scenario 5.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR6-View Related Findings - Main scenario
    @Test
    public void FR6_View_Related_Findings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR6-View Related Findings - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR7-View Related Incidents - Main scenario
    @Test
    public void FR7_View_Related_Incidents_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Bowtie Risk Assessment v5.2\\FR7-View Related Incidents - Main scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
